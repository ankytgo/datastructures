class TrieNode(object):
    
    def __init__(self, char=''):
        self.children = [None for _ in range(26)]
        self.char = char
        self.is_end_word = False
    
    def mark_as_leaf(self):
        self.is_end_word = True
        
    def unmark_as_leaf(self):
        self.is_end_word = False
        
class Trie(object):
    
    def __init__(self):
        self.root = TrieNode()
        
    def get_index(self, key):
        return ord(key) - ord('a')
    
    def insert(self, word):
        word = word.lower()
        current_node = self.root
        for l, c in enumerate(word):
            idx = self.get_index(c)
            if current_node.children[idx] is None:
                current_node.children[idx] = TrieNode(c)
            current_node = current_node.children[idx]
            
        current_node.mark_as_leaf()
    
    def search(self, word):
        word = word.lower()
        current_node = self.root
        for l, c in enumerate(word):
            idx = self.get_index(c)
            if current_node.children[idx] is None:
                return False
            current_node = current_node.children[idx]
            
        if not current_node.is_end_word:
            return False
        return True
            
    def delete(self, key):
        pass

if __name__ == '__main__':
	keys = ["the", "a", "there", "answer", "any", "by", "bye", "their", "abc"]
	output = ["Not present in the trie", "Present in the trie"]
	trie = Trie()
	for k in keys:
	    trie.insert(k)
	for k in ["the", "these", "abc"]:
	    print(k, output[trie.search(k)])
